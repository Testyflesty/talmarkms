const routes = [{
        path: '/',
        component: () =>
            import ('layouts/MainLayout.vue'),
        children: [{
            path: '',
            name: 'Home',
            component: () =>
                import ('pages/Home.vue')
        }]
    },
    {
        path: '/category/:id',
        name: 'Category',
        component: () =>
            import ('pages/PageCategory.vue'),
        props: true
    },
    {
        path: '/categories',
        name: 'Categories',
        component: () =>
            import ('pages/PageCategoryList.vue'),
        props: true
    },
    {
        path: '/forum/:id',
        name: 'Forum',
        component: () =>
            import ('pages/PageForum.vue'),
        props: true
    },
    {
        path: '/thread/:id',
        name: 'ThreadShow',
        component: () =>
            import ('pages/PageThreadShow.vue'),
        props: true
    },
    {
        path: '/thread/create/:forumId',
        name: 'ThreadCreate',
        component: () =>
            import ('pages/PageThreadCreate.vue'),
        props: true
    },
    {
        path: '/thread/:id/edit',
        name: 'ThreadEdit',
        component: () =>
            import ('pages/PageThreadEdit.vue'),
        props: true
    },
    {
        path: '/register',
        name: 'Register',
        component: () =>
            import ('pages/PageRegister.vue')
    },
    {
        path: '/signin',
        name: 'SignIn',
        component: () =>
            import ('pages/PageSignIn.vue')
    },
    {
        path: '/me',
        name: 'Profile',
        component: () =>
            import ('pages/PageProfile'),
        props: true
    },
    {
        path: '/me/edit',
        name: 'ProfileEdit',
        component: () =>
            import ('pages/PageProfile'),
        props: {
            edit: true
        }
    },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
    routes.push({
        path: '*',
        component: () =>
            import ('pages/Error404.vue')
    })
}

export default routes